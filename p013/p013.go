package p013

import (
	"bufio"
	"math/big"
	"strings"
)

// Large sum
// Problem 13
// Work out the first ten digits of the sum of
// the following one-hundred 50-digit numbers.

func First10Digits(bi big.Int) uint64 {

	lm := big.NewInt(9999999999)
	ten := big.NewInt(10)

	for bi.Cmp(lm) == 1 {
		bi.Quo(&bi, ten)
	}

	return bi.Uint64()
}

func SumBigNumsString(str string) big.Int {
	bigNums := ParseBigNums(str)
	return SumBigNums(bigNums)
}

func SumBigNums(nums []big.Int) (sum big.Int) {
	for _, n := range nums {
		sum.Add(&sum, &n)
	}
	return
}

func ParseBigNums(str string) (res []big.Int) {
	r := bufio.NewReader(strings.NewReader(str))
	res = []big.Int{}
	for i := 0; ; i++ {
		line, _, err := r.ReadLine()

		if err != nil {
			break
		}

		d, _ := parseLine(string(line))
		res = append(res, d)
	}

	return
}

func parseLine(l string) (res big.Int, success bool) {
	_, success = res.SetString(l, 10)
	return
}
