# Go solutions for Project Euler

[project euler](http://projecteuler.net)

Please note that all solutions might not be complete.

All problems are within their own package along with a test file
in which one of the tests prints the solution.