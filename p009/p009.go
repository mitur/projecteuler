package p009

// Project Euler
//
// Special Pythagorean triplet
// Problem 9
//
// A Pythagorean triplet is a set of three natural numbers, a < b < c,
// for which, a2 + b2 = c2
//
// For example, 32 + 42 = 9 + 16 = 25 = 52.
//
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.
//
// Finds and returns the pythagorean triplet whose sum is
// equal to the given sum
func PythTripletWithSum(sum int) (a, b, c int) {
	for a = 0; a < sum; a++ {
		for b = sum - a; b > 0; b-- {
			for c = sum - a - b; c > 0; c-- {
				if a+b+c == sum && isPythTriplet(a, b, c) {
					return
				}
			}
		}
	}
	return 0, 0, 0
}

func ProdOf(nums ...int) int {
	prod := 1
	for _, n := range nums {
		prod *= n
	}
	return prod
}

func isPythTriplet(a, b, c int) bool {
	return (a < b && b < c) && (a*a+b*b == c*c)
}
