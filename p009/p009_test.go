package p009

import (
	"fmt"
	"testing"
)

// Test file for problem 9

func TestSolve(t *testing.T) {
	a, b, c := PythTripletWithSum(1000)
	product := ProdOf(a, b, c)
	fmt.Printf("a:%d b:%d c:%d, abc=%d\n", a, b, c, product)
}
