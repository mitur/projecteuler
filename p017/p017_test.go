package p017

import (
	"fmt"
	"testing"
	"time"
)

// Number letter counts
// Problem 17
// If the numbers 1 to 5 are written out in words:
// one, two, three, four, five,
// then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

// If all the numbers from 1 to 1000 (one thousand) inclusive were
// written out in words, how many letters would be used?

// NOTE: Do not count spaces or hyphens. For example,
// 342 (three hundred and forty-two) contains 23 letters and
// 115 (one hundred and fifteen) contains 20 letters.
// The use of "and" when writing out numbers is in compliance
// with British usage.
type tup struct {
	n int
	s string
}

type FnNumToStr func(int) string

func TestLab(t *testing.T) {
	res := len("threehundredandfortytwo")
	fmt.Println(res)
}

func TestSolve(t *testing.T) {

	easyStart := time.Now()
	easyRes := easySolution()
	easyElapsed := time.Since(easyStart)
	fmt.Printf("%12s %d (%s)\n", "Easy Result:", easyRes, easyElapsed)

	start := time.Now()
	res := solution()
	elapsed := time.Since(start)
	fmt.Printf("%12s %d (%s)\n", "Result:", res, elapsed)

	fmt.Printf("Difference: %d - %d = %d\n", easyRes, res, easyRes-res)
}

func solution() (count int) {
	count = 0
	for i := 1; i < 1001; i++ {
		str := ToStr(i)
		l := len(str)
		//fmt.Printf("%s(len: %d)\n", str, l)
		count += l
	}
	return
}

func easySolution() int {
	// letter count for all unit numbers
	u := 3 + 3 + 5 + 4 + 4 + 3 + 5 + 5 + 4

	// letter count for all teen numbers
	t := 3 + 6 + 6 + 8 + 8 + 7 + 7 + 9 + 8 + 8

	// letter count for all denary numbers
	d := 10*(6+6+5+5+5+7+6+6) + 8*u

	// count for all hundred numbers
	h := u*100 + 9*(u+t+d) + 9*len("hundred") + 9*99*len("hundredand")

	/*
		fmt.Println("1-9:", u)
		fmt.Println("10-19:", t)
		fmt.Println("20-99:", d)
		fmt.Println("100-999:", h)
	*/
	return u + t + d + h + len("onethousand")
}

/*
func TestThousand(t *testing.T) {
	testTups(t,
		[]tup{
			tup{1000, "onethousand"},
			tup{1200, "onethousandtwohundred"},
		}, ToStr)
}


func TestToStr(t *testing.T) {
	testTups(t,
		[]tup{
			tup{1, "one"},
			tup{9, "nine"},
			tup{19, "nineteen"},
			tup{33, "thirtythree"},
			tup{100, "onehundred"},
			tup{101, "onehundredandone"},
			tup{110, "onehundredandten"},
			tup{115, "onehundredandfifteen"},
			tup{120, "onehundredandtwenty"},
			tup{199, "onehundredandnintynine"},
			tup{233, "twohundredandthirtythree"},
			tup{900, "ninehundred"},
			tup{999, "ninehundredandnintynine"},
		}, ToStr)
}

func TestHundred(t *testing.T) {
	testTups(t,
		[]tup{
			tup{100, "onehundred"},
			tup{101, "onehundredandone"},
			tup{190, "onehundredandninty"},
			tup{199, "onehundredandnintynine"},
			tup{200, "twohundred"},
			tup{233, "twohundredandthirtythree"},
			tup{301, "threehundredandone"},
			tup{564, "fivehundredandsixtyfour"},
			tup{777, "sevenhundredandseventyseven"},
			tup{899, "eighthundredandnintynine"},
			tup{894, "eighthundredandnintyfour"},
			tup{1, "!HUNDRED1"},
		}, hundred)
}

func TestDenary(t *testing.T) {
	testTups(t,
		[]tup{
			tup{10, "ten"},
			tup{15, "fifteen"},
			tup{19, "nineteen"},
			tup{20, "twenty"},
			tup{29, "twentynine"},
			tup{30, "thirty"},
			tup{33, "thirtythree"},
			tup{40, "forty"},
			tup{44, "fortyfour"},
			tup{89, "eightynine"},
			tup{99, "nintynine"},
			tup{100, "!DENARY100"},
			tup{1, "!DENARY1"},
		}, denary)
}

func testTups(t *testing.T, data []tup, fns ...FnNumToStr) {
	for _, fn := range fns {
		for _, val := range data {
			res := fn(val.n)
			if res != val.s {
				t.Errorf("\nFor %d:\n%12s %s\n%12s %s\n",
					val.n, "Expected:", val.s, "Got:", res)
			}
		}
	}
}
*/
