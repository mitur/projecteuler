package p017

import "fmt"

// Number letter counts
// Problem 17
// If the numbers 1 to 5 are written out in words:
// one, two, three, four, five,
// then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

// If all the numbers from 1 to 1000 (one thousand) inclusive were
// written out in words, how many letters would be used?

// NOTE: Do not count spaces or hyphens. For example,
// 342 (three hundred and forty-two) contains 23 letters and
// 115 (one hundred and fifteen) contains 20 letters.
// The use of "and" when writing out numbers is in compliance
// with British usage.

// This could have been done by just assigning a no letters to each number.
// i.e 1 = 3(one), 3 = 5(three)

// It's done this way since it is easier to visualize
var (
	unitStrs []string = []string{
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
	}

	teenStrs []string = []string{
		"ten",
		"eleven",
		"twelve",
		"thirteen",
		"fourteen",
		"fifteen",
		"sixteen",
		"seventeen",
		"eighteen",
		"nineteen",
	}

	denaryStrs []string = []string{
		"twenty",
		"thirty",
		"forty",
		"fifty",
		"sixty",
		"seventy",
		"eighty",
		"ninety",
	}
)

// Counts the number of letters in takes to write out all numbers
// in [from, to]
func NoLettersUpTo(from, to int) int {
	count := 0
	return count
}

func ToStr(num int) string {
	if 999999 < num {
		return fmt.Sprintf("Million not implemented%d", num)
	} else if 999 < num {
		return thousand(num)
	} else if 99 < num {
		return hundred(num)
	} else if 9 < num {
		return denary(num)
	} else {
		return unit(num)
	}
}

func thousand(num int) string {
	if num < 999 || 9999 < num {
		return fmt.Sprintf("!THOUSAND%d", num)
	}

	unitS := unit(num / 1000)

	if num%1000 == 0 {
		return fmt.Sprintf("%sthousand", unitS)
	} else {
		return fmt.Sprintf("%sthousand%s", unitS, ToStr(num%1000))
	}
}

// [100 - 999]
func hundred(num int) string {
	if num < 100 || 999 < num {
		return fmt.Sprintf("!HUNDRED%d", num)
	}

	unitS := unit(num / 100)

	if num%100 == 0 {
		return fmt.Sprintf("%shundred", unitS)
	} else {
		return fmt.Sprintf("%shundredand%s", unitS, ToStr(num%100))
	}
}

// [20-99]
func denary(num int) string {
	if num < 10 || 99 < num {
		return fmt.Sprintf("!DENARY%d", num)
	}

	// those dammed teens
	if 9 < num && num < 20 {
		return teenStrs[num%10]
	}

	denS := denaryStrs[(num/10)-2]
	unitD := num % 10

	if unitD == 0 {
		return denS
	} else {
		return fmt.Sprintf("%s%s", denS, unit(num%10))
	}
}

// Prints all numbers less than 10
// In reality all numbers [1-9]
func unit(num int) string {
	if 0 < num && num < 10 {
		return unitStrs[num-1]
	}

	return fmt.Sprintf("!UNIT%d", num)
}
