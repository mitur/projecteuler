package p011

import (
	"fmt"
	"testing"
)

// Test cases for project euler problem 11

func TestSolution(t *testing.T) {
	const correct int = 70600674

	res, winner := Solve()

	if res != correct {
		t.Errorf("Got %d\nExpected: %d\nDiff: %d\n",
			res, correct, correct-res)
	}

	fmt.Println(MarkedString(winner))
	fmt.Println("Product: ", res)
}
