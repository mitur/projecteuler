package p011

import (
	"bufio"
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

// Project Euler
// Largest product in a grid
// Problem 11
// In the 20×20 grid below, four numbers along a diagonal line have
// been marked in red.

const squareStr string = `08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48`

// The product of these numbers is 26 × 63 × 78 × 14 = 1788696.
// What is the greatest product of four adjacent numbers in the
// same direction (up, down, left, right, or diagonally) in the 20×20 grid?

const (
	dim int = 20 // 20 numbers per line, 20 lines: 20x20
)

type (
	TestFunc func(int, int) (int, Quad)
	Coord    [2]int
	Quad     [4]Coord
	Square   [dim][dim]int
)

var square Square = Square{}
var debug bool = false

var testFuncs []TestFunc = []TestFunc{
	calcColProd,
	calcLineProd,
	calcLDiagProd,
	calcRDiagProd,
}

func init() {
	r := bufio.NewReader(strings.NewReader(squareStr))
	for i := 0; i < dim; i++ {
		lb, _ := r.ReadString(byte('\n'))
		line := strings.Split(strings.Trim(lb, "\n"), " ")
		for j := 0; j < dim; j++ {
			num, err := strconv.Atoi(line[j])
			if err != nil {
				fmt.Println("Error when parsing squareString: ", err)
				return
			}
			square[i][j] = num
		}
	}
}

// Plan:
// step through all cells that are 2 steps away from 0
// and 1 step away from dim
// Create the diagonal, line and row sums.
// Iterate through the entire square and store the highest, simple
func Solve() (highestProd int, highestQuad Quad) {
	for y := 0; y < dim; y++ {
		for x := 0; x < dim; x++ {
			for fi := 0; fi < len(testFuncs); fi++ {
				prod, quad := testFuncs[fi](x, y)
				if prod > highestProd {
					highestProd = prod
					highestQuad = quad
				}
			}
		}
	}
	return
}

// Calculates the product of a line, returns 0 if x < 3
func calcLineProd(x, y int) (int, Quad) {
	if dim-4 < x {
		//fmt.Printf("calcLineProg bailing with cords x:%d y:%d\n", x, y)
		return 1, Quad{}
	}
	f1, f2, f3, f4 := square[y][x], square[y][x+1], square[y][x+2], square[y][x+3]
	return f1 * f2 * f3 * f4, Quad{
		Coord{x, y},
		Coord{x + 1, y},
		Coord{x + 2, y},
		Coord{x + 3, y},
	}

}

// Calculates the product of a row, returns 0 if y < 3
func calcColProd(x, y int) (int, Quad) {
	if dim-4 < y {
		//fmt.Printf("calcColProg bailing with cords x:%d y:%d\n", x, y)
		return 1, Quad{}
	}

	f1, f2, f3, f4 := square[y][x], square[y+1][x], square[y+2][x], square[y+3][x]

	return f1 * f2 * f3 * f4, Quad{
		Coord{x, y},
		Coord{x, y + 1},
		Coord{x, y + 2},
		Coord{x, y + 3},
	}
}

// Calculates the product of a diagonal row, returns 0 if y < 3 || x < 3
func calcRDiagProd(x, y int) (int, Quad) {

	if x+3 > 19 || y+3 > 19 {
		//fmt.Printf("calcDiagProg bailing with cords x:%d y:%d\n", x, y)
		return 1, Quad{}
	}

	f1, f2, f3, f4 := square[y][x], square[y+1][x+1], square[y+2][x+2], square[y+3][x+3]

	return f1 * f2 * f3 * f4, Quad{
		Coord{x, y},
		Coord{x + 1, y + 1},
		Coord{x + 2, y + 2},
		Coord{x + 3, y + 3},
	}
}

// Calculates the product of a diagonal row, returns 0 if y < 3 || x < 3
func calcLDiagProd(x, y int) (int, Quad) {

	if x-3 < 0 || y+3 > dim-1 {
		//fmt.Printf("calcDiagProg bailing with cords x:%d y:%d\n", x, y)
		return 1, Quad{}
	}

	f1, f2, f3, f4 := square[y][x], square[y+1][x-1], square[y+2][x-2], square[y+3][x-3]

	return f1 * f2 * f3 * f4, Quad{
		Coord{x, y},
		Coord{x - 1, y + 1},
		Coord{x - 2, y + 2},
		Coord{x - 3, y + 3},
	}
}

func (s Square) String() string {
	var buffer bytes.Buffer
	for i := 0; i < dim; i++ {
		for j := 0; j < dim; j++ {
			buffer.WriteString(fmt.Sprintf("%02d", square[i][j]))
			if j < dim-1 {
				buffer.WriteString(" ")
			}
		}
		if i < dim-1 {
			buffer.WriteString("\n")
		}
	}
	return buffer.String()
}

func MarkedString(q Quad) string {
	var buffer bytes.Buffer
	for i := 0; i < dim; i++ {
		for j := 0; j < dim; j++ {
			if q.hasCoord(j, i) {
				buffer.WriteString(fmt.Sprintf("[%02d]", square[i][j]))
			} else {
				buffer.WriteString(fmt.Sprintf(" %02d ", square[i][j]))
			}
		}
		if i < dim-1 {
			buffer.WriteString("\n")
		}
	}
	return buffer.String()
}

func (q *Quad) hasCoord(x, y int) bool {
	for _, tup := range q {
		if tup[0] == x && tup[1] == y {
			return true
		}
	}

	return false
}
