// Project Euler
// Problem 3

// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

package p003

const (
	number uint64 = 600851475143
)

type PrimeGen struct {
	curr   uint64
	primes []uint64
}

func FindPrimeFactors(d uint64) []uint64 {
	topStop := d / 2
	factors := []uint64{}

	g := NewPrimeGen()

	f := g.Next()
	iterations := 0

	for 1 < d && f < topStop {
		iterations++
		if d%f == 0 {
			factors = append(factors, f)
			d = d / f
		} else {
			f = g.Next()
		}
	}
	return factors
}

func NewPrimeGen() PrimeGen {
	return PrimeGen{curr: 1, primes: []uint64{}}
}

func (g *PrimeGen) Next() uint64 {
	if len(g.primes) == 0 {
		g.primes = append(g.primes, 2)
		return 2
	}

	for d := g.curr + 2; ; d = d + 2 {
		if g.isPrime(d) {
			g.curr = d
			g.primes = append(g.primes, d)
			return d
		}
	}
}

func (g *PrimeGen) isPrime(n uint64) bool {
	// the line that gets it going... :p
	if n == 2 {
		return true
	}

	max := quickSqrt(n)

	for _, v := range g.primes {
		if n%v == 0 {
			return false
		} else if max < v {
			return true
		}
	}
	return true
}

func isPrime(n uint64) bool {
	if n < 2 {
		return false
	}

	var d uint64 = 2
	for ; d < (n/2)+1; d++ {
		if n%d == 0 {
			return false
		}
	}

	return true
}

// Log2 returns log base 2 of n. It's the same as index of the highest
// bit set in n. n == 0 returns 0
func Log2(n uint64) uint64 {
	// Using uint instead of uint64 is about 25% faster
	// on x86 systems with the default Go compiler.
	var r, v uint
	if n < 1<<32 {
		v = uint(n)
	} else {
		r = 32
		v = uint(n >> 32)
	}
	if v >= 1<<16 {
		r += 16
		v >>= 16
	}
	if v >= 1<<8 {
		r += 8
		v >>= 8
	}
	if v >= 1<<4 {
		r += 4
		v >>= 4
	}
	if v >= 1<<2 {
		r += 2
		v >>= 2
	}
	r += v >> 1
	return uint64(r)
}

func quickSqrt(x uint64) uint64 {
	// The most significant bit of the square root of x is equal to log2(x)/2.
	// Using uint instead of uint64 guarantees native word size, which is
	// more than 60% faster on my x86 Netbook.
	r := uint(1 << (Log2(x) >> 1))
	s := r >> 1
	t := r + s
	// Try to set a bit in the intermediate result, see if the square of the
	// resulting number is smaller than the input. If not, set it as the new
	// intermediate result. Shift a bit to the right, repeat.
	for s > 0 {
		if uint64(t)*uint64(t) <= x {
			r = t
		}
		s >>= 1
		t = r + s
	}
	return uint64(r)
}
