// Project Euler
// Problem 3
// Tests of functions used
package p003

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

// func TestSpamPrimes(t *testing.T) {
// 	pg := NewPrimeGen()

// 	//noPrimes := 1000000
// 	noPrimes := 30
// 	startTime := time.Now()
// 	for i := 0; i < noPrimes; i++ {
// 		fmt.Println(pg.Next())
// 		//pg.Next()
// 	}
// 	elapsedTime := time.Since(startTime)

// 	fmt.Printf("Generating %d primes took %s\n", noPrimes, elapsedTime)

// }

func TestIsPrime(t *testing.T) {
	var sub uint64 = 7
	res := isPrime(sub)

	if !res {
		t.Errorf("%d is Prime", sub)
	}
}

func TestIsNotPrime(t *testing.T) {
	var notPrimes []uint64 = []uint64{4, 6, 12, 21, 30, 42, 64, 128, 111, 286223799}

	for _, v := range notPrimes {
		if isPrime(v) {
			t.Errorf("%d is NOT prime", v)
		}
	}
}

func TestManyNotPrimes(t *testing.T) {
	// Need to add 2, since 0 will fail
	max := 100000
	for i := 0; i < max; i++ {
		f1, f2 := rand.Intn(max)+2, rand.Intn(max)+2
		prod := uint64(f1 * f2)

		if isPrime(prod) {
			t.Errorf("%d should NOT be prime (factors: %d and %d", prod, f1, f2)
		}
	}
}

func TestNoFactors(t *testing.T) {
	var sub uint64 = 29
	res := FindPrimeFactors(sub)

	if len(res) > 0 {
		t.Errorf("%d should not have any factors, yet: %v\n", sub, res)
	}
}

func TestPrintSolution(t *testing.T) {
	res := FindPrimeFactors(number)
	fmt.Println("Found factors: ", res)
}
