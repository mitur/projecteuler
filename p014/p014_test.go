package p014

import (
	"fmt"
	"testing"
)

// Test for project euler problem 14
// Longest Collatz sequence

var (
	testSq1 []uint64 = []uint64{13, 40, 20, 10, 5, 16, 8, 4, 2, 1}
)

func TestLongestUpTo(t *testing.T) {
	upTo := 1000000
	res := LongestChainUpTo(upTo)

	fmt.Printf("Longest chain upto %d starts with %d\n", upTo, res.start)
	//fmt.Println(CollectCollatzSeq(int(res.start)))
}

func TestCollectSequence(t *testing.T) {
	expected := testSq1
	res := CollectCollatzSeq(13)

	if !eq(res, expected) {
		t.Errorf("\n%12s %v\n%12s %v\n",
			"Expected:", expected,
			"Got:", res)
	}
}

func TestFinishSeq(t *testing.T) {
	expected := len(testSq1)
	sq := NewCollatzSeq(int(testSq1[0]))
	res := int(sq.Finish())

	if res != expected {
		t.Errorf("\n%12s %d\n%12s %d\n",
			"Expected:", expected,
			"Got:", res)
	}

}

func eq(lh, rh []uint64) bool {
	if len(lh) != len(rh) {
		return false
	}

	for i := 0; i < len(lh); i++ {
		if lh[i] != rh[i] {
			return false
		}
	}
	return true
}
