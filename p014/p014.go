package p014

// Longest Collatz sequence
// Problem 14
// The following iterative sequence is defined for the set of positive integers:

// n → n/2 (n is even)
// n → 3n + 1 (n is odd)

// Using the rule above and starting with 13, we generate the following sequence:

// 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
// It can be seen that this sequence (starting at 13 and finishing at 1)
// contains 10 terms. Although it has not been proved yet (Collatz Problem),
// it is thought that all starting numbers finish at 1.

// Which starting number, under one million, produces the longest chain?
// NOTE: Once the chain starts the terms are allowed to go above one million.

type CollatzSeq struct {
	n, start, length uint64
}

func NewCollatzSeq(start int) CollatzSeq {
	if start < 1 {
		panic("Collatz sequence must start on digit > 0")
	}
	return CollatzSeq{n: 0, start: uint64(start), length: 0}
}

func LongestChainUpTo(upTo int) CollatzSeq {
	s := CollatzSeq{}
	var longest CollatzSeq = CollatzSeq{length: 1}
	for i := 1; i < upTo; i++ {
		s.ResetWith(i)
		if longest.length < s.Finish() {
			longest = s
		}

	}

	return longest
}

// Runs the sequence until it is finished and returns the length
func (cs *CollatzSeq) Finish() uint64 {
	for !cs.Done() {
		cs.Next()
	}
	return cs.length
}

func CollectCollatzSeq(start int) (res []uint64) {
	g := NewCollatzSeq(start)

	for d := g.Next(); ; d = g.Next() {
		res = append(res, d)
		if d == 1 {
			break
		}
	}

	return
}

func (cq *CollatzSeq) Done() bool {
	return cq.length > 0 && cq.n == 1
}

func (cq *CollatzSeq) ResetWith(start int) {
	cq.n = 0
	cq.length = 0
	cq.start = uint64(start)
}

// Returns the next number in the sequence
func (cq *CollatzSeq) Next() (res uint64) {
	if cq.length == 0 {
		res = cq.start
	} else if cq.n%2 == 0 {
		res = cq.n / 2
	} else {
		res = 3*cq.n + 1
	}
	cq.n = res
	cq.length++
	return
}
