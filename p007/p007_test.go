package p007

import (
	"fmt"
	"testing"
)

func TestSolution(t *testing.T) {
	res := FindPrimeNr(10001)

	fmt.Println("the 10001th prime is: ", res)
}
