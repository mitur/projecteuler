package p007

import "bitbucket.org/mitur/projecteuler/p003"

// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
// we can see that the 6th prime is 13.
// What is the 10 001st prime number?

// Finds the Prime with number
func FindPrimeNr(nr int64) int64 {
	g := p003.NewPrimeGen()

	var prime int64

	var i int64 = 0
	for ; i < nr; i++ {
		prime = g.Next()
	}

	return prime
}
