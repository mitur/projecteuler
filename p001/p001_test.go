// Project Euler
// Problem 1

// Tests
package main

import "testing"

const (
	correctAnswer int = 233168
)

func TestSolver(t *testing.T) {
	res := Solve()

	if res != correctAnswer {
		t.Errorf("Expected: %d\nGot: %d\n", correctAnswer, res)
	}
}
