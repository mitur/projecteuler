package p019

import "fmt"

// Month
const (
	JAN = 0 + iota
	FEB
	MAR
	APR
	MAY
	JUN
	JUL
	AUG
	SEP
	OCT
	NOV
	DEC
)

// Weekdays
const (
	MONDAY = 0 + iota
	TUESDAY
	WEDNESDAY
	THURSDAY
	FRIDAY
	SATURDAY
	SUNDAY
)

var dayNames []string = []string{
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday",
}

func SolveIter() int {
	count := 0    // count of sundays that fell on the first of the month
	day := MONDAY // startDay
	for year := 1901; year < 2001; year++ {
		for month := JAN; month < 12; month++ {
			if day == SUNDAY {
				count++
			}
			// The first of next month will be
			// Subtract one since we are standing on the first of the month
			day = (day + DaysInMonth(month, year) - 1) % 7

			fmt.Printf("Year %d, month %d was a %s\n", year, month, toDayString(day))

		}
		//fmt.Printf("Year %d: %d\n", year, count)
	}
	return count
}

func toDayString(day int) string {
	if 0 <= day && day < 7 {
		return fmt.Sprintf(dayNames[day])
	} else {
		panic(fmt.Sprintf("UNKNOWN DAY %d", day))
	}
}

// Returns the number of days in the given month
// where 0 = January and 11 = December
// for the years in [1901-2000]
// so there is only one century 2000, which is divis by 400
// So all years % 4 == 2 is leap years.
func DaysInMonth(month, year int) int {
	if month < 0 || 11 < month {
		panic(fmt.Sprintf("INVALID MONTH %d", month))
	}
	// Special case FEB
	if month == 1 {
		if year%4 == 0 && (year%1000 != 0 || year%400 == 0) {
			return 29
		} else {
			return 28
		}
	}

	if month == 3 || month == 5 || month == 8 || month == 10 {
		// April, June, September and November
		return 30
	} else {
		// All the rest
		return 31
	}

}
