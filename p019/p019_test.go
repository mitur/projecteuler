// p019_test.go
package p019

import (
	"fmt"
	"testing"
)

func TestSolution(t *testing.T) {
	res := SolveIter()

	fmt.Println(res, "sundays fell on the first of the month in [1901-2000]")
}

func TestDaysInMonth(t *testing.T) {
	type Test struct{ month, year, exp int }

	tests := []Test{
		Test{JAN, 1999, 31},
		Test{JUL, 1999, 31},
		Test{APR, 1999, 30},
		Test{JUN, 1999, 30},
		Test{SEP, 1999, 30},
		Test{NOV, 1999, 30},
		Test{DEC, 1999, 31},
		Test{FEB, 1999, 28},
		Test{FEB, 1901, 28},
		Test{FEB, 1912, 29},
		Test{FEB, 2000, 29},
	}

	for _, test := range tests {
		res := DaysInMonth(test.month, test.year)

		if res != test.exp {
			t.Errorf("Month %d %d should have %d days, got %d",
				test.month, test.year, test.exp, res)
		}

	}
}
