package p020

import (
	"fmt"
	"math/big"
	"testing"
	"time"
)

func TestSolve(t *testing.T) {
	start := time.Now()
	res := SumDigits(DigitsInNum(Fac(100)))
	elapsed := time.Since(start)

	fmt.Printf("Found Solution: %d in %s\n", res, elapsed)

}

func TestExamples(t *testing.T) {
	in := 10
	exp := 27

	res := SumDigits(DigitsInNum(Fac(in)))

	if res != exp {
		t.Errorf("\nFor input %d:\nReceived: %d\nExpected: %d", in, res, exp)
	}
}

func TestFac(t *testing.T) {
	tests := []struct {
		v   int
		exp int64
	}{
		{v: 2, exp: 2},
		{v: 3, exp: 6},
		{v: 4, exp: 24},
		{v: 5, exp: 120},
		{v: 10, exp: 3628800},
	}

	for _, c := range tests {
		res := Fac(c.v)

		if res.Int64() != c.exp {
			t.Errorf("\nFor input %d:\nReceived: %d\nExpected: %d",
				c.v, res.Int64(), c.exp)
		}
	}
}

func TestDigitsInNum(t *testing.T) {
	tests := []struct {
		v   *big.Int
		exp []int
	}{
		{v: big.NewInt(123), exp: []int{1, 2, 3}},
		{v: big.NewInt(1), exp: []int{1}},
		{v: big.NewInt(0), exp: []int{0}},
		{v: big.NewInt(1234), exp: []int{1, 2, 3, 4}},
		{v: big.NewInt(12340), exp: []int{1, 2, 3, 4, 0}},
	}

	for _, c := range tests {
		res := DigitsInNum(c.v)

		if !slizeEq(res, c.exp) {
			t.Errorf("\nFor input %s:\nReceived: %v\nExpected: %v", c.v, res, c.exp)
		}
	}
}

func slizeEq(s1, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}

	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}
