package p020 // problem 20. Sum of digits in 100!

import (
	"fmt"
	"math/big"
)

// Solve solves problem 20
func Solve() int {
	return SumDigits(DigitsInNum(Fac(100)))
}

// Fac takes an integer and returns the factorial of that integer
func Fac(n int) (res *big.Int) {
	res = big.NewInt(1)
	for 1 < n {
		res = res.Mul(res, big.NewInt(int64(n)))
		n--
	}
	return
}

// DigitsInNum takes a large integer and
// returns all digits within that integer as a slice
func DigitsInNum(n *big.Int) (res []int) {
	b10 := big.NewInt(10)

	if n.BitLen() < 6 && n.Int64() < 10 {
		res = []int{int(n.Int64())}
		fmt.Println("Quicky")
		return
	}

	mod := &big.Int{}

	for 0 < n.BitLen() {
		n, _ = n.DivMod(n, b10, mod)
		res = append(res, int(mod.Int64()))
	}

	l := len(res)
	hl := l / 2

	for i := 0; i < hl; i++ {
		temp := res[i]
		res[i] = res[l-1-i]
		res[l-1-i] = temp
	}

	return
}

// SumDigits takes a slize of ints and returns the sum of them
func SumDigits(ints []int) (res int) {
	for _, d := range ints {
		res += d
	}
	return
}
