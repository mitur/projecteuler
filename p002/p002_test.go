// Project Euler
// Problem 1
// Testing
package p002

import "testing"

func TestFibGenerator(t *testing.T) {
	var g FibGen
	var d int
	for g = NewFibGen(); d < 4000000; {
		prev, curr := g.prev, g.curr
		d = g.Next()
		if prev+curr != d {
			t.Errorf("\nprev: %d\ncurr: %d\nres: %d\n prev+curr != res",
				prev, curr, d)
		}
	}
}
