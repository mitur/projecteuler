package p018

import (
	"fmt"
	"testing"
)

const testStr1 string = `75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23`

const testStr2 string = `3
7 4
2 4 6
8 5 9 3`

var (
	smallP *Pyramid
	bigP   *Pyramid
)

func TestHighTrav(t *testing.T) {
	res, tp := bigP.getHighTrav()

	fmt.Println(tp)

	fmt.Println("Hight traverse:", res)

}

func TestCalcNoRowsInvalid(t *testing.T) {

	tests := []int{
		2, 4, 5, 7, 9, 11, 14, 20, 22, 35, 37,
	}

	for _, v := range tests {
		if _, err := calcNoRows(v); err == nil {
			t.Errorf("Expected error for input %d", v)
		}

	}

}

func TestCalcNoRows(t *testing.T) {
	type TUnit struct {
		arg, exp int
	}

	tests := []TUnit{
		TUnit{1, 1},
		TUnit{3, 2},
		TUnit{6, 3},
		TUnit{10, 4},
		TUnit{15, 5},
		TUnit{21, 6},
		TUnit{28, 7},
		TUnit{36, 8},
	}

	for _, tst := range tests {
		res, err := calcNoRows(tst.arg)
		if err != nil {
			t.Errorf("Caught error when eval %#v, error: %s\n", tst, err)
		}

		if res != tst.exp {
			t.Errorf("Got %d\nExpected %d\nFor test %#v\n", res, tst.exp, tst)
		}

	}

}

// 0:  75
// 1:  95 64
// 2:  17 47 82
// 3:  18 35 87 10
// 4:  20 04 82 47 65
// 5:  19 01 23 75 03 34
// 6:  88 02 77 73 07 63 67
// 7:  99 65 04 28 06 16 70 92
// 8:  41 41 26 56 83 40 80 70 33
// 9:  41 48 72 33 47 32 37 16 94 29
// 10: 53 71 44 65 25 43 91 52 97 51 14
// 11: 70 11 33 28 77 73 17 78 39 68 17 57
// 12: 91 71 52 38 17 14 91 43 58 50 27 29 48
// 13: 63 66 04 68 89 53 67 30 73 16 69 87 40 31
// 14: 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

func TestGet(t *testing.T) {
	type Cs struct{ row, ri int }
	type Test struct {
		arg Cs
		exp int
	}

	tests := []Test{
		Test{Cs{0, 0}, 75},
		Test{Cs{1, 0}, 95},
		Test{Cs{4, 1}, 4},
		Test{Cs{13, 0}, 63},
		Test{Cs{14, 14}, 23},
	}

	for _, val := range tests {
		res := bigP.get(val.arg.row, val.arg.ri)

		if res != val.exp {
			t.Errorf("row%d index%d should be %d, got %d\n",
				val.arg.row, val.arg.ri, val.exp, res)
		}
	}
}

func TestNoRows(t *testing.T) {
	res1 := bigP.noRows
	res2 := smallP.noRows

	if res1 != 15 {
		t.Errorf("Big pyramid should have 15 rows, got: %d\n", res1)
	}

	if res2 != 4 {
		t.Errorf("small pyramid should have 4 rows, got: %d\n", res2)
	}
}

func TestRowLn(t *testing.T) {
	type Test struct {
		arg, exp int
	}
	tests := []Test{
		Test{0, 1},
		Test{2, 3},
		Test{6, 7},
		Test{14, 15},
		Test{15, -1},
	}

	for _, v := range tests {
		res := bigP.rowLen(v.arg)

		if res != v.exp {
			t.Errorf("Length of row %d, should be %d, got %d\n",
				v.arg, v.exp, res)
		}
	}
}

func init() {
	p, err := PyramidFromStr(testStr1)

	if err != nil {
		fmt.Printf("Caught error when parsing big pyramid: %s\n", err)
	}

	bigP = p

	p, err = PyramidFromStr(testStr2)

	if err != nil {
		fmt.Printf("Caught error when parsing small pyramid: %s\n", err)
	}

	smallP = p

}
