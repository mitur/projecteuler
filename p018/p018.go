package p018

import (
	"bufio"
	"bytes"
	"fmt"
	"math"
	"strconv"
	"strings"
)

// Not working!
type Pyramid struct {
	v      []int
	noRows int
}

type TraversedPyramid struct {
	*Pyramid

	// The Path chosen
	// Stores the index for each row, i.e:
	// a 0 on index 1 means we went via the first element
	// on the 2nd row.
	Path []int
}

// The TraversedPyramid returned is incorrect.
func (p Pyramid) getHighTrav() (int, *TraversedPyramid) {
	newV := make([]int, len(p.v), cap(p.v))
	np := &Pyramid{v: newV, noRows: p.noRows}

	copy(np.v, p.v)

	// Create the sum
	for r := np.noRows - 1; r > 0; r-- {
		rowLn := np.rowLen(r)
		for ri := 0; ri+1 < rowLn; ri++ {
			left, right := np.get(r, ri), np.get(r, ri+1)
			if left > right {
				np.set(r-1, ri, left+np.get(r-1, ri))
			} else {
				np.set(r-1, ri, right+np.get(r-1, ri))
			}
		}
	}

	fmt.Println(np)

	// Create the Path.
	// Start from the top and select the highest sum
	// in each row.
	tp := &TraversedPyramid{
		Pyramid: &p,
		Path:    make([]int, p.noRows, p.noRows),
	}

	for r := 0; r < np.noRows; r++ {
		rowLn := p.rowLen(r)
		big := np.get(r, 0) // the current biggest el in this row
		chosen := 0         // the chosen index. Will be added to Paths

		for i := 0; i < rowLn; i++ {
			el := np.get(r, i)
			if el > big {
				big = el
				chosen = i
			}
		}
		tp.Path[r] = chosen
	}

	return np.get(0, 0), tp
}

func (p *Pyramid) getI(i int) int {
	return p.v[i]
}

func (p *Pyramid) get(row, rowI int) int {
	rowEnd := (row*row + row) / 2
	res := p.v[rowEnd+rowI]
	return res
}

func (p *Pyramid) set(row, rowI, val int) {
	rowEnd := (row*row + row) / 2
	p.v[rowEnd+rowI] = val
}

// Returns negative if row is out of bounds
func (p *Pyramid) rowLen(row int) int {
	if 0 <= row && row < p.noRows {
		return row + 1
	}

	return -1

}

func (p *Pyramid) len() int {
	return len(p.v)
}

func NewPyramid(nums []int) (*Pyramid, error) {

	// junk with in sqrt

	noRows, err := calcNoRows(len(nums))

	if err != nil {
		return nil, err
	}

	return &Pyramid{v: nums, noRows: noRows}, nil

}

// Takes the number of elements and returns the number of
// rows that can be created and an error.
// The error will be set if a valid pyramid cannot be created
// using the noEl
func calcNoRows(noEl int) (int, error) {
	// f(x) = no rows where x = no elements
	// f(x) = sqrt(1/4 + 2x) - 1/2
	// multiply with the 2 from the formula
	// and with 4 to be able to add the forth
	// that is also in the sqrt
	t1 := noEl*4*2 + 1
	sqrtRes := math.Sqrt(float64(t1))
	res := (sqrtRes - 1) / 2

	rm := math.Remainder(sqrtRes-1, 2)

	if rm != 0 {
		// Cannot create pyramid from noEl blocks.
		return -1, fmt.Errorf("Cannot create a valid pyramid from %d elements", noEl)
	}

	return int(res), nil

}

func (p Pyramid) String() string {
	b := bytes.NewBuffer([]byte(""))

	for row := 0; row < p.noRows; row++ {
		rowLn := p.rowLen(row)

		for i := p.noRows - row; i > 0; i-- {
			b.WriteString("  ")
		}
		for rowI := 0; rowI < rowLn; rowI++ {
			b.WriteString(fmt.Sprintf(" %02d ", p.get(row, rowI)))

		}
		b.Write([]byte("\n"))
	}
	return b.String()
}

func (tp *TraversedPyramid) TraversedVia(row, rowi int) bool {
	return row < len(tp.Path) &&
		row < 00 &&
		rowi < tp.rowLen(row) &&
		tp.Path[row] == rowi
}

func (tp *TraversedPyramid) String() string {
	b := bytes.NewBuffer([]byte(""))

	for row := 0; row < tp.noRows; row++ {
		rowLn := tp.rowLen(row)
		fmt.Printf("row %d len: %d\n", row, rowLn)

		for i := tp.noRows - row; i > 0; i-- {
			b.WriteString("  ")
		}

		for rowI := 0; rowI < rowLn; rowI++ {
			if tp.Path[row] == rowI {
				b.WriteString(fmt.Sprintf("[%02d]", tp.get(row, rowI)))
			} else {
				b.WriteString(fmt.Sprintf(" %02d ", tp.get(row, rowI)))
			}

		}
		b.Write([]byte("\n"))
	}

	b.WriteString(fmt.Sprintf("Path: %v\n", tp.Path))

	return b.String()
}

func PyramidFromStr(s string) (*Pyramid, error) {
	scanner := bufio.NewScanner(strings.NewReader(s))
	scanner.Split(bufio.ScanWords)
	var res []int

	for scanner.Scan() {
		x, err := strconv.Atoi(scanner.Text())

		if err != nil {
			return nil, fmt.Errorf("Caught error when scanning pyramid: %s", err)
		}

		res = append(res, x)
	}

	p, err := NewPyramid(res)

	if err != nil {
		return nil, fmt.Errorf("Could not create a pyramid from scanned string: %s", err)
	}

	return p, scanner.Err()

}
