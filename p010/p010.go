package p010

import "bitbucket.org/mitur/projecteuler/p003"

// p010.go
// Project Euler
// Summation of primes
// Problem 10
// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//
// Find the sum of all the primes below two million.

func SumPrimesTo(to int) int64 {
	g := p003.NewPrimeGen()

	var d int64 = 0
	var sum int64 = 0
	for ; d < int64(to); d = g.Next() {
		sum += d
	}
	return sum
}
