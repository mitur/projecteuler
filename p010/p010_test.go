// p010_test.go
// Test cases for project euler problem 10

package p010

import (
	"fmt"
	"testing"
)

func TestSolution(t *testing.T) {
	const sumTo int = 2000000
	sum := SumPrimesTo(sumTo)

	fmt.Printf("Sum of all primes < %d: %d\n", sumTo, sum)
}
