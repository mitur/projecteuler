package p015

// Lattice paths
// Problem 15
// Starting in the top left corner of a 2×2 grid,
// and only being able to move to the right and down,
// there are exactly 6 routes to the bottom right corner.
// How many such routes are there through a 20×20 grid?

// Solution explained
// 3x3
// ---- ---- ----
// | 2| | 3| | 4|
// ---- ---- ----
// ---- ---- ----
// | 3| | 6| |10|
// ---- ---- ----
// ---- ---- ----
// | 4| |10| |20|
// ---- ---- ----

// noPaths(x, y) = noPaths(x-1, y) + noPaths(x, y-1)
// noPaths(1, 1) = 2
func Solve(dim int) int {
	grid := make([][]int, dim, dim)
	for i := 0; i < dim; i++ {
		grid[i] = make([]int, dim, dim)
	}

	for y := 0; y < dim; y++ {
		for x := 0; x < dim; x++ {
			if x == 0 && y == 0 { // first row and column
				grid[y][x] = 2
			} else if y == 0 && 0 < x { // first row
				grid[y][x] = grid[y][x-1] + 1
			} else if x == 0 && 0 < y { // first column
				grid[y][x] = grid[y-1][x] + 1
			} else {
				grid[y][x] = grid[y-1][x] + grid[y][x-1]
			}

		}
	}

	return grid[dim-1][dim-1]

}
