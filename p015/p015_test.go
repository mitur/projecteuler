package p015

import (
	"fmt"
	"testing"
	"time"
)

// Tests for project euler problem 15
// Lattice paths
// Problem 15
// Starting in the top left corner of a 2×2 grid,
// and only being able to move to the right and down,
// there are exactly 6 routes to the bottom right corner.
// How many such routes are there through a 20×20 grid?

/*func TestSolution(t *testing.T) {
	res := RecSolve(20)

	fmt.Println("No paths through 20x20 grid: ", res)
}*/

func TestDim3(t *testing.T) {
	res := Solve(3)
	expected := 20

	if res != expected {
		t.Errorf("Expected %d, Got %d\n", res, expected)
	}
}

func TestDim4(t *testing.T) {
	res := Solve(4)
	expected := 70

	if res != expected {
		t.Errorf("Expected %d, Got %d\n", res, expected)
	}
}

func TestSolve(t *testing.T) {
	start := time.Now()
	res := Solve(20)
	elapsed := time.Since(start)

	fmt.Printf("Possible paths through 20x20: %d (%s)\n", res, elapsed)
}
