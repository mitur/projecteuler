package p008

import (
	"fmt"
	"testing"
)

func TestSolve(t *testing.T) {
	const correctRes int64 = 23514624000
	facs, prod := Solve()

	if prod != correctRes {
		t.Errorf("Expected %d\nGot %d\n", correctRes, prod)
	} else {
		fmt.Println("Highest product: ", prod, ", Created by: ", facs)
	}
}

func TestNumSpitter(t *testing.T) {
	ns := NewNumSpitter("1234")
	var res int = 0
	var err error
	if res, err = ns.Next(); res != 1 && err != nil {
		t.Errorf("Expected 1, got: %d, error: %v", res, err)
	}

	if res, err = ns.Next(); res != 2 && err != nil {
		t.Errorf("Expected 2, got: %d, error: %v", res, err)
	}

	if res, err = ns.Next(); res != 3 && err != nil {
		t.Errorf("Expected 3, got: %d, error: %v", res, err)
	}

	if res, err = ns.Next(); res != 4 && err != nil {
		t.Errorf("Expected 4, got: %d, error: %v", res, err)
	}
}
