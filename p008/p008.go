package p008

import (
	"bufio"
	"fmt"
	"strings"
)

// The four adjacent digits in the 1000-digit number that have the
// greatest product are 9 × 9 × 8 × 9 = 5832.

const square string = `
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450`

// Find the thirteen adjacent digits in the 1000-digit number
// that have the greatest product. What is the value of this product?

type NumSpitter struct {
	pos int
	str string
}

type Box [13]int64

// Finds the biggest product of 13 consecutive numbers from the square above
func Solve() (*Box, int64) {
	ns := NewNumSpitter(trimLineFeeds())

	// holds the largest product
	var highestProd int64 = 1 // Holds the current highest product
	var nums Box = Box{}      // Holds teh current 13 nums
	var highestFacs *Box      // Holds the 13 digits that created the highest product

	// read the first 13 numbers
	for i := 0; i < 13; i++ {
		d, err := ns.Next()

		if err != nil {
			fmt.Println("Early error: ", err)
			return nil, 0
		}

		nums[i] = int64(d)
		highestProd *= int64(d)

	}

	highestFacs = nums.Copy()

	var d int
	var err error
	for ; err == nil; d, err = ns.Next() {
		nums.ShiftAdd(int64(d))
		newProd := nums.Prod()
		if highestProd < newProd {
			highestProd = newProd
			highestFacs = nums.Copy()
		}
	}

	return highestFacs, highestProd

}

func (s *NumSpitter) Next() (int, error) {
	if s.pos < len(s.str) {
		res := int(rune(s.str[s.pos]) - '0')
		s.pos++
		return res, nil
	} else {
		return -1, fmt.Errorf("Out of string")
	}
}

func (ds *Box) Prod() int64 {
	var res int64 = 1
	for i := 0; i < len(ds); i++ {
		res *= ds[i]
	}
	return res
}

// Shifts everything down and adds n to the last
// index. Returns the first digit (removed)
func (ds *Box) ShiftAdd(n int64) int64 {
	first := ds[0]
	for i := 1; i < len(ds); i++ {
		ds[i-1] = ds[i]
	}
	ds[12] = n
	return first
}

func (ds *Box) Copy() *Box {
	nb := Box{}
	for i := 0; i < len(ds); i++ {
		nb[i] = ds[i]
	}
	return &nb
}

// Creates a new NumSpitter from given String
// the String will attempt to eat numbers one by one
// from the given String, when the string is out or no
// numbers can be found it will return an error.
func NewNumSpitter(str string) NumSpitter {
	return NumSpitter{pos: 0, str: str}
}

func trimLineFeeds() string {
	rdr := bufio.NewReader(strings.NewReader(square))

	res := ""

	for {
		bs, isPrefix, err := rdr.ReadLine()

		if err != nil {
			return res
		}
		if !isPrefix {
			res = res + string(bs)
		}
	}
}
