// p004.go
// Project Euler, problem 4
package main

import "fmt"

func main() {
	facs, res := Solve(1000)

	fmt.Printf("%d * %d = %d\n", facs[0], facs[1], res)

}

// Returns the largest palindrome that can be made using
// two numbers smaller than factorMax.
// Also returns the two factors
func Solve(factorMax int64) ([]int64, int64) {
	var (
		f1, f2  int64
		largest int64   = 0
		facts   []int64 = []int64{0, 0}
	)

	for f1 = 0; f1 < factorMax; f1++ {
		for f2 = 0; f2 < factorMax; f2++ {
			prod := f1 * f2
			if IsPalindrome(prod) && prod > largest {
				facts[0] = f1
				facts[1] = f2
				largest = prod
			}
		}
	}

	return facts, largest
}

// Checks if a given number is a palindrome
// or not
func IsPalindrome(d int64) bool {
	if d < 10 {
		return true
	}

	orig := d
	var reversed int64 = 0

	for orig > 0 {
		reversed = (reversed * 10) + orig%10
		orig = orig / 10
	}
	return d == reversed
}
