// p004_test.go
// Project Euler, problem 4
// Test file
package main

import "testing"

func TestIsPalindrome(t *testing.T) {
	var sub int64 = 1234
	res := IsPalindrome(sub)
	if res {
		t.Errorf("%d is not a palindrome", sub)
	}

	sub = 0
	res = IsPalindrome(sub)
	if !res {
		t.Errorf("%d is a palindrome", sub)
	}

	sub = 12321
	res = IsPalindrome(sub)
	if !res {
		t.Errorf("%d is a palindrome", sub)
	}
}

// Tests the solving method
func TestSolve(t *testing.T) {
	const maxFactor int64 = 1000
	const expected int64 = 906609

	facs, res := Solve(maxFactor)

	if res != expected {
		t.Errorf("Received result: %d, expected: %d, factors: %v", res, expected, facs)
	}
}
