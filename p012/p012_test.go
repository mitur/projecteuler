package p012

import (
	"fmt"
	"testing"
	"time"
)

func TestSolution(t *testing.T) {
	start := time.Now()
	res := FirstWithNDivs(500)
	elapsed := time.Since(start)

	fmt.Printf("First with > 500 divs: %d (%d divisors)\n",
		res.num, len(res.divs))
	fmt.Println("Solution took", elapsed)
}

func TestFirstWithNDivs(t *testing.T) {
	res := FirstWithNDivs(5)

	if res.num != 28 {
		fmt.Printf("Expected 28, got: %d\n", res.num)
	}
}

func TestGetFirstNWithDivs(t *testing.T) {
	res := FirstNWithDivs(7)

	if len(res) != 7 {
		fmt.Println("Expected 7 numbers back, got:", len(res))
	}
}
