// p005.go
// Project Euler, problem 4
// 2520 is the smallest number that can be divided by each of the
// numbers from 1 to 10 without any remainder.
// What is the smallest positive number that is evenly divisible
// by all of the numbers from 1 to 20?
package p005

func Solve() int64 {
	var n int64 = 1
	for ; ; n++ {
		if check(n) {
			return n
		}
	}
}

// Checks if num is evenly divisable by all numbers
// between 1 and 20
func check(num int64) bool {
	var i int64 = 20
	for ; 1 < i; i-- {
		if num%i != 0 {
			return false
		}
	}
	return true
}

func factorial(n int) int {
	if n == 1 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}
